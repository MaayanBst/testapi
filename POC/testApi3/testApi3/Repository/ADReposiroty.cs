﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using System.IO;
using testApi3.Models;

namespace testApi3.Repository {
    public class ADReposiroty : IADRepository {
        public void logsToFile(string log, bool status) {

            string currentStatus = "Error";
            if (status) {
                currentStatus = "Info";
            }
            string filePath = @"C:\testApi\log.txt";
            string[] currentLog = { DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss") + $" [{currentStatus}:] " + log + "\n" };

            if (File.Exists(filePath)) {
                File.AppendAllLines(filePath, currentLog);
            } else {
                using (var stream = File.Create(filePath)) {
                    using (TextWriter tw = new StreamWriter(stream)) {
                        tw.WriteLine(currentLog);
                    }
                }
            }

        }
    }
}
