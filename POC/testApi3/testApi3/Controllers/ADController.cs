﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using testApi3.Models;
using testApi3.Services;

namespace testApi3.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class ADController : Controller
    {
        private readonly IADClass _adclass;

        public ADController(IADClass activeDirectoryInterface) {
            _adclass = activeDirectoryInterface;
        }

        [HttpGet("changeDisplayName")]

        public string Get(string userName, string newDisplayName)
        {
            return _adclass.changeDisplayName(userName, newDisplayName);
        }

        [HttpPost("changeHierarchy")]

        public string Post(string userName, string ouName)
        {
            return _adclass.changeHierarchy(userName, ouName);
        }

        [HttpPut("disableUser")]

        public string disableUser(string userName)
        {
            return _adclass.disableUser(userName);
        }

        [HttpPut("enableUser")]

        public string enableUser(string userName)
        {
            return _adclass.enableUser(userName);
        }

        [HttpDelete("addToGroup")]

        public string addToGroup(string userName, string groupName)
        {
            return _adclass.addToGroup(userName, groupName);
        }

        [HttpDelete("removeFromGroup")]

        public string removeFromGroup(string userName, string groupName)
        {
            return _adclass.removeFromGroup(userName, groupName);
        }
    }
}
