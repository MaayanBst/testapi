﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace testApi3.Models {
    public interface IUtils {

        string getDistinguishedName(string userName);
        string getOuDistinguishedName(string ouName);
        string getDistinguishedNameWithCN(string userName);
        bool groupMembers(string groupName, string userDn);
    }
}
