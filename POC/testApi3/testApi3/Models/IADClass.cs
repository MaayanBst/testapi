﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace testApi3.Models
{
    public interface IADClass
    {
        string removeFromGroup(string userName, string groupName);

        string addToGroup(string userName, string groupName);

        string changeDisplayName(string userName, string newDisplayName);

        string disableUser(string userName);

        string enableUser(string userName);

        string changeHierarchy(string userName, string ouName);
    }
}
