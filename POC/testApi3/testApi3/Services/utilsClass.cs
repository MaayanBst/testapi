﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Threading.Tasks;
using testApi3.Models;

namespace testApi3.Services {
    public class utilsClass : IUtils {

        public string getDistinguishedName(string userName) {

            var entry = new DirectoryEntry("LDAP://SDI.lab/DC=SDI,DC=lab");
            var searcher = new DirectorySearcher(entry) { Filter = $"SamAccountName={userName}" };

            try {
                var searchResult = searcher.FindOne();

                if (searchResult != null) {

                    string distinguishedName = searchResult.Properties["distinguishedName"][0].ToString();

                    if (distinguishedName.Contains("CN")) {
                        var distinguishedNameWithCn = distinguishedName.Split(new[] { ',' }, 2);
                        distinguishedName = distinguishedNameWithCn[1];
                    }
                    return distinguishedName;
                } else {
                    return "not Found";
                }

            } catch (System.Runtime.InteropServices.COMException exception) {
                return "not Found";
            }

        }

        public string getOuDistinguishedName(string ouName) {

            var entry = new DirectoryEntry("LDAP://SDI.lab/DC=SDI,DC=lab");
            var searcher = new DirectorySearcher(entry) { Filter = $"name={ouName}" };

            try {

                var searchResult = searcher.FindOne();
                if (searchResult != null) {
                    string distinguishedName = searchResult.Properties["distinguishedName"][0].ToString();
                    return distinguishedName;
                } else {
                    return "not Found";
                }
            } catch (System.Runtime.InteropServices.COMException exception) {
                return "not Found";
            }

        }

        public string getDistinguishedNameWithCN(string userName) {

            var entry = new DirectoryEntry("LDAP://SDI.lab/DC=SDI,DC=lab");
            var searcher = new DirectorySearcher(entry) { Filter = $"SamAccountName={userName}" };

            try {

                var searchResult = searcher.FindOne();
                if (searchResult != null) {
                    string distinguishedName = searchResult.Properties["distinguishedName"][0].ToString();
                    return distinguishedName;
                } else {
                    return "not Found";
                }
            } catch (System.Runtime.InteropServices.COMException exception) {
                return "not Found";
            }

        }

        public bool groupMembers(string groupName, string userDn) {
            string allMembers = "";
            var entry = new DirectoryEntry("LDAP://SDI.lab/DC=SDI,DC=lab");
            var searcher = new DirectorySearcher(entry) { Filter = $"SamAccountName={groupName}" };
            try {
                var searchResult = searcher.FindOne();

                if (searchResult != null) {
                    foreach (string member in searchResult.Properties["member"]) {
                        allMembers += " " + member;
                    }
                    if (allMembers.Contains(userDn)) {
                        return true;
                    }
                }

            } catch (System.Runtime.InteropServices.COMException exception) {
                return false;
            }
            return false;
        }

    }
}
