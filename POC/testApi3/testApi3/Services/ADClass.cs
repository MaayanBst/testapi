﻿using System;
using testApi3.Models;
using System.DirectoryServices;
using testApi3.Repository;
using System.Collections.Generic;

namespace testApi3.Services
{
    public class ADClass : IADClass
    {
        private readonly IADRepository _Repository;

        private readonly IUtils _Utils;

        public ADClass(IADRepository activeDirectoryRepository, IUtils utils) {
            _Repository = activeDirectoryRepository;
            _Utils = utils;
        }

        public string removeFromGroup(string userName, string groupName)
        {
            string log = "";
            bool status;
            string groupDn = _Utils.getDistinguishedNameWithCN(groupName);
            string userDn = _Utils.getDistinguishedNameWithCN(userName);

            if (groupDn != "not Found" && userDn != "not Found") {
                try {
                    DirectoryEntry dirEntry = new DirectoryEntry("LDAP://" + groupDn);
                    DirectorySearcher searcher = new DirectorySearcher(dirEntry);

                    if (!_Utils.groupMembers(groupName, userDn)) {
                        log = userName + " is not part of " + groupName;
                        status = false;
                        _Repository.logsToFile(log, status);
                        return log;
                    }
                    dirEntry.Properties["member"].Remove(userDn);
                    dirEntry.CommitChanges();
                    dirEntry.Close();
                    log = userName + " was removed from " + groupName;
                    status = true;
                } catch (System.Runtime.InteropServices.COMException e) {
                    status = false;
                }
            } else {
                log = "Unable to find user or group distinguishedName";
                status = false;
            }

            _Repository.logsToFile(log, status);
            return log;
        }

        public string addToGroup(string userName, string groupName)
        {
            string log = "";
            bool status;
            string groupDn = _Utils.getDistinguishedNameWithCN(groupName);
            string userDn = _Utils.getDistinguishedNameWithCN(userName);

            if (groupDn != "not Found" && userDn != "not Found") {
                try {
                    DirectoryEntry dirEntry = new DirectoryEntry("LDAP://" + groupDn);
                    DirectorySearcher searcher = new DirectorySearcher(dirEntry);

                    if (_Utils.groupMembers(groupName, userDn)) {
                        log = userName + " is already part of " + groupName;
                        status = false;
                        _Repository.logsToFile(log, status);
                        return log;
                    }
                    dirEntry.Properties["member"].Add(userDn);
                    dirEntry.CommitChanges();
                    dirEntry.Close();
                    log = userName + " was added to group " + groupName;
                    status = true;
                } catch (System.Runtime.InteropServices.COMException e) {
                    log = "the user was not found";
                    status = false;
                }
            } else {
                log = "Unable to find user or group distinguishedName";
                status = false;
            }

            _Repository.logsToFile(log, status);
            return log;
        }

        public string changeDisplayName(string userName, string newDisplayName)
        {
            string log = "";
            bool status;
            string distinguishedName = _Utils.getDistinguishedName(userName);

            if (distinguishedName != "not Found") {
                try {
                    DirectoryEntry dirEntery = new DirectoryEntry($"LDAP://{distinguishedName}");
                    DirectorySearcher dirSearcher = new DirectorySearcher(dirEntery);
                    foreach (DirectoryEntry child in dirEntery.Children) {

                        if ((string)child.Properties["SamAccountName"].Value == userName) {

                            string oldDisplayName = child.Properties["DisplayName"].Value.ToString();
                            child.Properties["DisplayName"].Value = newDisplayName;
                            child.CommitChanges();
                            log = userName + $" old displayName was {oldDisplayName} and {userName} new one is " + newDisplayName;
                            status = true;
                            _Repository.logsToFile(log, status);
                            return log;
                        }
                    }
                    log = userName + " user was not found";
                    status = false;
                    dirEntery.Close();
                } catch (System.Runtime.InteropServices.COMException e) {
                    log = "error accured while changing displayName";
                    status = false;
                }
            } else {
                log = "Unable to find user or ou distinguishedName";
                status = false;
            }

            _Repository.logsToFile(log, status);
            return log;
        }

        public string disableUser(string userName)
        {
            string log = "";
            bool status;
            string distinguishedName = _Utils.getDistinguishedName(userName);

            if (distinguishedName != "not Found") {

                try {
                    DirectoryEntry dirEntery = new DirectoryEntry($"LDAP://{distinguishedName}");

                    DirectorySearcher dirSearcher = new DirectorySearcher(dirEntery);

                    foreach (DirectoryEntry child in dirEntery.Children) {

                        if ((string)child.Properties["SamAccountName"].Value == userName) {

                            int flags = (int)child.Properties["userAccountControl"].Value;
                            bool userStatus = !Convert.ToBoolean(flags & 0x0002);
                            if (!userStatus) {
                                log = userName + " is already disabled";
                                status = false;
                                _Repository.logsToFile(log, status);
                                return log;
                            }
                            int val = (int)child.Properties["userAccountControl"].Value;
                            child.Properties["userAccountControl"].Value = val | 0x2;
                            child.CommitChanges();
                            log = userName + " is disabled";
                            status = true;
                            _Repository.logsToFile(log, status);
                            return log;
                        }
                    }
                    log = userName + " was not found";
                    status = false;
                } catch (System.Runtime.InteropServices.COMException e) {
                    log = "error accured while disabling user";
                    status = false;
                }
            } else {
                log = "Unable to find user or ou distinguishedName";
                status = false;
            }

            _Repository.logsToFile(log, status);
            return log;
        }

        public string enableUser(string userName)
        {
            string log = "";
            bool status;
            string distinguishedName = _Utils.getDistinguishedName(userName);

            if (distinguishedName != "not Found") {
                try {
                    DirectoryEntry dirEntery = new DirectoryEntry($"LDAP://{distinguishedName}");

                    DirectorySearcher dirSearcher = new DirectorySearcher(dirEntery);

                    foreach (DirectoryEntry child in dirEntery.Children) {

                        if ((string)child.Properties["SamAccountName"].Value == userName) {

                            int flags = (int)child.Properties["userAccountControl"].Value;
                            bool userStatus = !Convert.ToBoolean(flags & 0x0002);

                            if (userStatus) {
                                log = userName + " is already enabled";
                                status = false;
                                _Repository.logsToFile(log, status);
                                return log;
                            }
                            int val = (int)child.Properties["userAccountControl"].Value;
                            child.Properties["userAccountControl"].Value = val & ~0x2;
                            child.CommitChanges();
                            log = userName + " is enabled";
                            status = true;
                            _Repository.logsToFile(log, status);
                            return log;
                        }
                    }

                    log = userName + " was not found";
                    status = false;
                } catch (System.Runtime.InteropServices.COMException e) {
                    log = "error accured while enabling user";
                    status = false;
                }
            } else {
                log = "Unable to find user or OU distinguishedName";
                status = false;
            }

            _Repository.logsToFile(log, status);
            return log;
        }

        public string changeHierarchy(string userName, string ouName)
        {
            string log = "";
            bool status;
            string distinguishedName = _Utils.getDistinguishedNameWithCN(userName);
            string ouDistinguishedName = _Utils.getOuDistinguishedName(ouName);

            if (distinguishedName.Contains(ouName)) {
                log = userName + " is already part of " + ouName;
                status = false;
                _Repository.logsToFile(log, status);
                return log;
            }
            if (distinguishedName != "not Found" && ouDistinguishedName != "not Found") {
                DirectoryEntry eLocation = new DirectoryEntry($"LDAP://{distinguishedName}");
                DirectoryEntry nLocation = new DirectoryEntry($"LDAP://{ouDistinguishedName}");

                if (eLocation != null && nLocation != null) {
                    eLocation.MoveTo(nLocation);
                    nLocation.Close();
                    eLocation.Close();
                    log = userName + " was successfully moved to " + ouName;
                    status = true;
                } else {
                    log = $"Unable to move {userName} to new localtion";
                    status = false;
                }
            } else {
                log = "Unable to find user or ou distinguishedName";
                status = false;
            }

            _Repository.logsToFile(log, status);
            return log;
        }

    }

}
