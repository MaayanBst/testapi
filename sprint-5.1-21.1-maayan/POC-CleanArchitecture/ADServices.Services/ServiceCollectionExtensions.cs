﻿using ADServices.Services;
using ADServices.Services.Imp;

using System;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddSingleton<IADUsersService, ADUsersService>();

            return services;
        }
    }
}
