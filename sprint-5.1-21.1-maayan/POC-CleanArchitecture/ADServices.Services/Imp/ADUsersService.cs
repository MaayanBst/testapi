﻿
using ADServices.Data.Repositories;

using Microsoft.Extensions.Logging;

using System;

namespace ADServices.Services.Imp
{
    class ADUsersService : IADUsersService
    {
        private readonly ILogger<ADUsersService> _logger;
        private readonly IUsersRepository _usersRepository;

        public ADUsersService(ILogger<ADUsersService> logger, IUsersRepository usersRepository)
        {
            _logger = logger;
            _usersRepository = usersRepository;
        }

        public bool checkIdentity(string userName) {
            return _usersRepository.checkIdentity(userName);
        }

        public bool IsValidUser(string user)
        {
            return _usersRepository.IsUserValid(user);
        }

        public bool resetPassword(string userName, string newPassword) {
            return _usersRepository.resetPassword(userName, newPassword);
        }

        public bool unlock(string userName) {
            return _usersRepository.unlock(userName);
        }

    }
}
