﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADServices.Services
{
    public interface IADUsersService
    {
        bool IsValidUser(string user);
        bool checkIdentity(string userName);
        bool resetPassword(string userName, string newPassword);
        bool unlock(string userName);
    }
}
