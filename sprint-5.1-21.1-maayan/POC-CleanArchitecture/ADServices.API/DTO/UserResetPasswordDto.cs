﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ADServices.API.DTO
{
    public class UserResetPasswordDto
    {
        public string userName { get; set; }
        public string newPassword { get; set; }
    }
}
