﻿using ADServices.API.DTO;
using ADServices.Services;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace ADServices.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ADUsersController : ControllerBase
    {
        private readonly ILogger<ADUsersController> _logger;
        private readonly IADUsersService _adUsersService;

        public ADUsersController(ILogger<ADUsersController> logger, IADUsersService adUsersService)
        {
            _logger = logger;
            _adUsersService = adUsersService;
        }

        [HttpGet("{userName}")]
        public IActionResult Get(string userName)
        {
            if (!_adUsersService.IsValidUser(userName))
                return BadRequest($"user:{userName} is not a valid user for the given operation");

            if (!_adUsersService.checkIdentity(userName)) {
                return BadRequest($"user:{userName} identity is not set properly");
            }

            return Ok();
        }

        [HttpPut("resetPassword")]
        public IActionResult resetPassword([FromBody] UserResetPasswordDto UserResetPasswordDto)
        {          
            
            if(!_adUsersService.IsValidUser(UserResetPasswordDto.userName))
                return BadRequest($"user:{UserResetPasswordDto.userName} is not a valid user for the given operation");

            if(!_adUsersService.resetPassword(UserResetPasswordDto.userName, UserResetPasswordDto.newPassword))
            {
                return BadRequest($"unable to reset password for user: {UserResetPasswordDto.userName}");
            }


            return Ok();
        }

        [HttpPut("unlock")]
        public IActionResult unlock([FromBody] userToUnlockDto userToUnlockDto) {

            if (!_adUsersService.IsValidUser(userToUnlockDto.userName))
                return BadRequest($"user:{userToUnlockDto.userName} is not a valid user for the given operation");

            if (!_adUsersService.unlock(userToUnlockDto.userName)) {
                return BadRequest($"unable to unlock the user: {userToUnlockDto.userName}");
            }


            return Ok();
        }
    }
}
