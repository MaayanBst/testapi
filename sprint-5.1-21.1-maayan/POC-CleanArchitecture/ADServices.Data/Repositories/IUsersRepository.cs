﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADServices.Data.Repositories
{
    public interface IUsersRepository {
        bool checkIdentity(string userName);
        bool IsUserValid(string user);
        bool resetPassword(string userName, string newPassword);
        bool unlock(string userName);
    }
}
