﻿using System.DirectoryServices;
using ADServices.Data.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Management;

namespace ADServices.Data
{
    internal class UsersRepository : IUsersRepository
    {
        private string _ldapServerUrl;
        public UsersRepository(ILogger<UsersRepository> logger, IConfiguration config)
        {
            _ldapServerUrl = config.GetConnectionString("LDAP");
        }

        public bool checkIdentity(string userName) {

            bool status = false;
            string mailAttribute = "";

            try {
                DirectoryEntry directoryEntry = new DirectoryEntry();
                DirectorySearcher directorySearcher = new DirectorySearcher(directoryEntry, $"(&(objectClass=user)(SamAccountName={userName}))");
                DirectoryEntry userDirectoryEntry = new DirectoryEntry(directorySearcher.FindOne().Path);

                mailAttribute = userDirectoryEntry.Properties["mail"].Value.ToString();

                directorySearcher.Dispose();
                directoryEntry.Dispose();
                directoryEntry.Close();
                userDirectoryEntry.Dispose();
                userDirectoryEntry.Close();
            } catch (System.Runtime.InteropServices.COMException e) {
                status = false;
            }
            if (mailAttribute.Contains("@")) {
                var mailAttributeSplit = mailAttribute.Split(new[] { '@' }, 2);
                mailAttribute = mailAttributeSplit[1];

                if (mailAttribute.Contains("d360")) {
                    status = true;
                }
            }

            return status;
        }

        public bool IsUserValid(string user) {
            var directoryEntry = new DirectoryEntry("LDAP://SDI.lab/DC=SDI,DC=lab");
            var directorySearcher = new DirectorySearcher(directoryEntry) { Filter = $"SamAccountName={user}" };

            try {
                var searchResult = directorySearcher.FindOne();

                if (searchResult != null) {

                    return true;
                } else {
                    return false;
                }

            } catch (System.Runtime.InteropServices.COMException exception) {
                return false;
            }

            directoryEntry.Close();
            directoryEntry.Dispose();
            directorySearcher.Dispose();
        }

        public bool resetPassword(string userName, string newPassword) {
            bool status = false;

            try {
                DirectoryEntry directoryEntry = new DirectoryEntry();
                DirectorySearcher directorySearcher = new DirectorySearcher(directoryEntry, $"(&(objectClass=user)(SamAccountName={userName}))");
                DirectoryEntry userDirectoryEntry = new DirectoryEntry(directorySearcher.FindOne().Path);

                userDirectoryEntry.Invoke("SetPassword", new object[] { newPassword });
                status = true;

                directorySearcher.Dispose();
                directoryEntry.Dispose();
                directoryEntry.Close();
                userDirectoryEntry.Dispose();
                userDirectoryEntry.Close();
            } catch (System.Runtime.InteropServices.COMException e) {
                status = false;
            }

            return status;
        }

        public bool unlock(string userName) {
            bool status = false;

            try {
                DirectoryEntry directoryEntry = new DirectoryEntry();
                DirectorySearcher directorySearcher = new DirectorySearcher(directoryEntry, $"(&(objectClass=user)(SamAccountName={userName}))");
                DirectoryEntry userDirectoryEntry = new DirectoryEntry(directorySearcher.FindOne().Path);

                userDirectoryEntry.Properties["LockOutTime"].Value = 0;
                status = true;

                directorySearcher.Dispose();
                directoryEntry.Dispose();
                directoryEntry.Close();
                userDirectoryEntry.Dispose();
                userDirectoryEntry.Close();
            } catch (System.Runtime.InteropServices.COMException e) {
                status = false;
            }

            return status;
        }
    }
}
