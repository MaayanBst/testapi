﻿using System;

namespace Extensions
{
    public static class StringExtensions
    {
        public static string ConcatOperation(this string value, string operation)
        {
            return string.Concat(value, operation);
        }
    
    }
}
