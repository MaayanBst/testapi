﻿using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection.PortableExecutable;
using WebApplication1.Models;
using DirectoryEntry = System.DirectoryServices.DirectoryEntry;
using System.Globalization;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace WebApplication1.Services
{

    class AD : ADInterface
    {

        public bool changeDN(string username, string displayName)
        {
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain, "sdi.lab");

            UserPrincipal up = UserPrincipal.FindByIdentity(ctx, username);
            
            if (up != null)
            {
                string DN = up.DisplayName;
                up.DisplayName = displayName;
                up.Save();
                return true;
                
            }

            return false;
        }

        public bool disableUser()
        {
            // set up domain context
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain, "sdi.lab");

            // find a user
            UserPrincipal user = UserPrincipal.FindByIdentity(ctx, "Danas");

            if (user != null)
            {
                user.Enabled = false;
                user.Save();
                return true;
            }

            return false;

        }

        public bool setPassword()
        {
            // set up domain context
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain, "sdi.lab");

            // find a user
            UserPrincipal user = UserPrincipal.FindByIdentity(ctx, "MayanB");

            if (user != null)
            {
                user.ChangePassword("Aa123456?", "aA123456?");
                user.Save();
                return true;
            }

            return false;
        }

        public bool addToADGroup()
        {
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain, "sdi.lab");

            GroupPrincipal gp = GroupPrincipal.FindByIdentity(ctx, IdentityType.Name, "testOU");

            UserPrincipal user = UserPrincipal.FindByIdentity(ctx, "MayanB");

            if (gp != null && user != null )
            {
                gp.Members.Add(ctx, IdentityType.Name, "Mayan Bar Siman Tov");
                gp.Save();
                gp.Dispose();
                return true;
            }

            ctx.Dispose();
            return false;
        }

        public bool addToLocalGroup()
        {
            PrincipalContext ctx = new PrincipalContext(ContextType.Machine, "SDI-Client03");

            PrincipalContext ctxDomain = new PrincipalContext(ContextType.Domain, "sdi.lab");

            GroupPrincipal gp = GroupPrincipal.FindByIdentity(ctx, IdentityType.Name, "Administrators");

            UserPrincipal user = UserPrincipal.FindByIdentity(ctxDomain, "MayanB");

            if (gp != null && user != null)
            {
                gp.Members.Add(user);
                gp.Save();
                gp.Dispose();
                return true;
            }

            ctx.Dispose();
            return false;
        }

        public bool addToLocalGroupDifferentComputer()
        {
            PrincipalContext ctx = new PrincipalContext(ContextType.Machine, "SDI-Client01");

            PrincipalContext ctxDomain = new PrincipalContext(ContextType.Domain, "sdi.lab");

            GroupPrincipal gp = GroupPrincipal.FindByIdentity(ctx, IdentityType.Name, "Administrators");

            UserPrincipal user = UserPrincipal.FindByIdentity(ctxDomain, "asafl@sdi.lab");

            

            return true;
        }

        public bool changeGroupName()
        {
            PrincipalContext context = new PrincipalContext(ContextType.Domain, "sdi.lab");
            GroupPrincipal group = GroupPrincipal.FindByIdentity(context, "TestAdmin");
            var Groupentry = (DirectoryEntry)group.GetUnderlyingObject();

            if (Groupentry != null && group != null)
            {
                string newName = "testNew";
                Groupentry.Rename($"cn={newName}");
                Groupentry.CommitChanges();
                return true;
            }

            return false;
        }

        public string dir()
        {
            //PrincipalContext ctx = new PrincipalContext(ContextType.Domain, "sdi.lab");
            //UserPrincipal user = UserPrincipal.FindByIdentity(ctx, "MayanB");

            //string bla = user.DistinguishedName;
            DirectoryEntry eLocation = new DirectoryEntry("LDAP://CN=Hila Zahirir,OU=Sapir,OU=Enabled Users,OU=User Accounts,DC=SDI,DC=lab");
            DirectoryEntry nLocation = new DirectoryEntry("LDAP://OU=mador440,OU=Sapir,OU=Enabled Users,OU=User Accounts,DC=SDI,DC=lab");
            eLocation.MoveTo(nLocation);
            nLocation.Close();
            eLocation.Close();
            return "";
        }

        public string testLDAP()
        {
            
            string groupDn = getDistinguishedName("testAdmin");
            string userDn = getDistinguishedName("AsafL");
            try {
                DirectoryEntry dirEntry = new DirectoryEntry("LDAP://" + groupDn);
                DirectorySearcher searcher = new DirectorySearcher(dirEntry);
                //dirEntry.Properties["member"];
                dirEntry.CommitChanges();
                dirEntry.Close();
                return "good";
            } catch (System.DirectoryServices.DirectoryServicesCOMException E) {
                return E.ToString();
            }
        }

        public string getDistinguishedName(string userName) {

            var entry = new DirectoryEntry("LDAP://SDI.lab/DC=SDI,DC=lab");
            var searcher = new DirectorySearcher(entry) { Filter = $"SamAccountName={userName}" };

            foreach (DirectoryEntry child in entry.Children) {
                if ((string)child.Properties["SamAccountName"].Value == userName) {
                    return "good";
                } else {
                    return "bad";
                }
            }

            return "bla";
            try {
                var sr = searcher.FindOne();
                string distinguishedName = sr.Properties["distinguishedName"][0].ToString();

                //if (distinguishedName.Contains("CN")) {
                //    var bla = distinguishedName.Split(new[] { ',' }, 2);
                //    distinguishedName = bla[1];
               // }
                return distinguishedName;
            } catch (Exception exception) {
                return "not Found";
            }

        }

        public bool userGroups(string userName, string groupName) {

            string distinguishedName = getDistinguishedName(userName);

            DirectoryEntry dirEntery = new DirectoryEntry($"LDAP://{distinguishedName}");
            DirectorySearcher dirSearcher = new DirectorySearcher(dirEntery);

            foreach (DirectoryEntry child in dirEntery.Children) {

                if ((string)child.Properties["SamAccountName"].Value == userName) {

                    string memberOf = child.Properties["memberOf"].Value.ToString();
                    if (memberOf.Contains(groupName)) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }

            return false;
        }

        public void copyFile(string remote, string local, bool status) {
            if (File.Exists(local)) {
                File.Copy(remote, local, status);
            } else {
                using (var stream = File.Create(local)) {
                    using (TextWriter tw = new StreamWriter(stream)) {
                        File.Copy(remote, local, status);
                    }
                }
            }
            
        }

        public void cpoyPath1(string local, string remote) {
            using (FileStream localDest = File.OpenWrite(local))
            using (FileStream remoteSource = File.OpenRead(remote)) {
                remoteSource.CopyTo(localDest);
            }
        }
    }
}


