﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Services
{

    interface Itest
    {
        string testGetMethod();
        int testPostMethod(int val);
        string testDeleteMethod();
        string testPutMethod();

    }

    class test : Itest
    {
        public string testGetMethod()
        {
            return "get request response";
        }

        public int testPostMethod(int val)
        {
            return (val)*(val);
        }

        public string testDeleteMethod()
        {
            return "delete request response";
        }

        public string testPutMethod()
        {
            return "put request response";
        }
    }

}
