﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Services;

namespace WebApplication1.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class testController : Controller
    {

        Itest test1 = new test();

        [HttpGet]
        public string Get()
        {
            return test1.testGetMethod();
        }

        [HttpPost]

        public int Post(int id)
        {
            return test1.testPostMethod(id);
        }

        [HttpPut]
        public string Put()
        {
            return test1.testPutMethod();
        }

        [HttpDelete]
        public string Delete()
        {
            return test1.testDeleteMethod();
        }
    }
}
