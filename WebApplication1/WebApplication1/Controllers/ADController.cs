﻿using System;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.Services;
using System.Management;
using System.Threading;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ADController : Controller
    {
        ADInterface ADClass = new AD();
        [HttpGet("bla")]
        public ContentResult Bla() {

            string hostname = "SDI-DC";
            string fileName = $"OneTest-{hostname}-" + DateTime.Now.ToString("MM-dd-yyyy");
            string fileExtension = "html";
            string localPath = $"C:\\{fileName}.{fileExtension}";
            var processToRun = new[] { @"C:\onetest-autoFile.exe" };

            // defines the connection properties to remote computer
            ConnectionOptions con = new ConnectionOptions();
            con.Impersonation = ImpersonationLevel.Impersonate;
            con.Authentication = AuthenticationLevel.Default;
            con.EnablePrivileges = true;

            // looks for the remote pc and make the connection
            ManagementScope scope = new ManagementScope(new ManagementPath(@$"\\{hostname}\root\cimv2"), con);
            scope.Connect();
            ManagementClass testClass = new ManagementClass(scope, new ManagementPath("Win32_Process"), new ObjectGetOptions());
            // starts the process on remote pc
            testClass.InvokeMethod("Create", processToRun);
            Thread.Sleep(10000);

            // looks for the onetest report on remote pc
            ObjectQuery query = new ObjectQuery($"SELECT * FROM CIM_DataFile WHERE FileName = '{fileName}' AND Extension = '{fileExtension}'");
            ManagementObjectSearcher  searcher = new ManagementObjectSearcher(scope, query);
            string remotePath = @$"\\{hostname}\C$\Users\SDIAdmin\Desktop\{fileName}.{fileExtension}";

            foreach (ManagementObject managementObject in searcher.Get()) {
                // copy the remote file to local one
                ADClass.copyFile(remotePath, localPath, true);
            }
            return base.Content(System.IO.File.ReadAllText(localPath), "text/html"); ;
        }

    }
}