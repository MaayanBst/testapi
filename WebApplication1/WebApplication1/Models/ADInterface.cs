﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    interface ADInterface
    {
        bool changeDN(string username, string displayName);

        void copyFile(string path1, string path2, bool status);

        void cpoyPath1(string local, string remote);
        bool disableUser();

        bool setPassword();

        bool addToADGroup();

        bool addToLocalGroup();

        bool addToLocalGroupDifferentComputer();

        bool changeGroupName();

        string dir();

        string testLDAP();

        bool userGroups(string userName, string groupName);
    }
}
