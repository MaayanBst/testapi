﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace WebApplication1.middlewares
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class testMiddleware
    {
        private readonly RequestDelegate _next;

        public testMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {

            var currentUL = httpContext.Request.GetEncodedPathAndQuery();
            string[] paramVal = currentUL.Split('=');
            bool isParam = false;

            if (paramVal.Length > 1)
            {
                isParam = true;
            }

            if (httpContext.Request.Method == "POST") {

                if (isParam)
                {
                    if (paramVal[1] != "0")
                    {
                        await _next(httpContext);
                    }
                    else
                    {
                        await httpContext.Response.WriteAsync("[Error]: id can't be equal 0");
                    }
                } else
                {
                    await httpContext.Response.WriteAsync("[Error]: POST request need params");
                }

            } else if (httpContext.Request.Method == "GET")
            {
                if (!isParam)
                {
                    await _next(httpContext);
                } else
                {
                    await httpContext.Response.WriteAsync("[Error]: no params allown in GET http request");
                }
                
            } else
            {
                await _next(httpContext);
            }

        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class testMiddlewareExtensions
    {
        public static IApplicationBuilder UsetestMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<testMiddleware>();
        }
    }
}
