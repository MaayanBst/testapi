﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADServices.Data.Repositories {
    public interface IGroupsUsersRepository {
        bool IsGroupValid(string groupName);
        bool addToGroup(string name, string groupName);
        bool removeFromGroup(string name, string groupName);
        string getDistinguishedNameWithCN(string userName);
    }
}
