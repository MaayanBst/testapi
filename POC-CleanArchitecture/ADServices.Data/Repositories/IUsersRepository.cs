﻿namespace ADServices.Data.Repositories
{
    public interface IUsersRepository {
        bool updateAttribute(string user, string attributeVal, string attribute);

        bool isUserValid(string user);
        bool enableUser(string name);
        bool disableUser(string name);
    }
}
