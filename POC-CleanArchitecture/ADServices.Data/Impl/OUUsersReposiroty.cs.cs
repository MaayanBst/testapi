﻿using System.DirectoryServices;
using ADServices.Data.Repositories;
using Microsoft.Extensions.Logging;

namespace ADServices.Data.Impl {
    class OUUsersReposiroty : IOUUsersReposiroty {

        private readonly ILogger<OUUsersReposiroty> _logger;
        private readonly IGroupsUsersRepository _GroupsUsersRepository;

        public OUUsersReposiroty(ILogger<OUUsersReposiroty> logger, IGroupsUsersRepository GroupsUsersRepository) {
            _logger = logger;
            _GroupsUsersRepository = GroupsUsersRepository;
        }

        public bool hierarchy(string name, string ouName) {
            bool status;
            string distinguishedName = _GroupsUsersRepository.getDistinguishedNameWithCN(name);
            string ouDistinguishedName = getOuDistinguishedName(ouName);

            if (distinguishedName.Contains(ouName)) {
                status = false;
                return status;
            }
            if (distinguishedName != "not Found" && ouDistinguishedName != "not Found") {
                DirectoryEntry eLocation = new DirectoryEntry($"LDAP://{distinguishedName}");
                DirectoryEntry nLocation = new DirectoryEntry($"LDAP://{ouDistinguishedName}");

                if (eLocation != null && nLocation != null) {
                    eLocation.MoveTo(nLocation);
                    nLocation.Close();
                    eLocation.Close();
                    status = true;
                } else {
                    status = false;
                }
            } else {
                status = false;
            }
            return status;
        }

        public bool isValidOu(string ouName) {
            DirectoryEntry entry = new DirectoryEntry("LDAP://SDI.lab/DC=SDI,DC=lab");
            DirectorySearcher searcher = new DirectorySearcher(entry) { Filter = $"name={ouName}" };

            try {
                var searchResult = searcher.FindOne();

                if (searchResult != null) {

                    return true;
                } else {
                    return false;
                }

            } catch (System.Runtime.InteropServices.COMException exception) {
                return false;
            }

            entry.Close();
            searcher.Dispose();
        }

        private string getOuDistinguishedName(string ouName) {

            var entry = new DirectoryEntry("LDAP://SDI.lab/DC=SDI,DC=lab");
            var searcher = new DirectorySearcher(entry) { Filter = $"name={ouName}" };

            try {

                var searchResult = searcher.FindOne();
                if (searchResult != null) {
                    string distinguishedName = searchResult.Properties["distinguishedName"][0].ToString();
                    return distinguishedName;
                } else {
                    return "not Found";
                }
            } catch (System.Runtime.InteropServices.COMException exception) {
                return "not Found";
            }

            entry.Close();
            searcher.Dispose();
        }
    }
}
