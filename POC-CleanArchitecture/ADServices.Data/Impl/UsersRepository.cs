﻿using ADServices.Data.Repositories;
using System.DirectoryServices;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace ADServices.Data.Impl
{
    internal class UsersRepository : IUsersRepository
    {
        private string _ldapServerUrl;
        public UsersRepository(ILogger<UsersRepository> logger, IConfiguration config)
        {
            _ldapServerUrl = config.GetConnectionString("LDAP");
        }

        public bool updateAttribute(string user, string attributeVal, string attribute) {

            bool status = false;
            string distinguishedName = getDistinguishedName(user);

            if (distinguishedName != "not Found") {
                try {
                    DirectoryEntry dirEntery = new DirectoryEntry($"LDAP://{distinguishedName}");
                    DirectorySearcher dirSearcher = new DirectorySearcher(dirEntery);
                    foreach (DirectoryEntry child in dirEntery.Children) {

                        if ((string)child.Properties["SamAccountName"].Value == user) {

                            child.Properties[attribute].Value = attributeVal;
                            child.CommitChanges();
                            status = true;
                        }
                    }
                    dirEntery.Close();
                    dirSearcher.Dispose();
                } catch (System.Runtime.InteropServices.COMException e) {
                    status = false;
                }
            } else {
                status = false;
            }

            return status;
        }

        public bool enableUser(string name) {
            bool status = false;
            string distinguishedName = getDistinguishedName(name);

            if (distinguishedName != "not Found") {
                try {
                    DirectoryEntry dirEntery = new DirectoryEntry($"LDAP://{distinguishedName}");
                    DirectorySearcher dirSearcher = new DirectorySearcher(dirEntery);

                    foreach (DirectoryEntry child in dirEntery.Children) {

                        if ((string)child.Properties["SamAccountName"].Value == name) {
                            int val = (int)child.Properties["userAccountControl"].Value;
                            child.Properties["userAccountControl"].Value = val & ~0x2;
                            child.CommitChanges();
                            status = true;
                        }
                    }
                    dirSearcher.Dispose();
                } catch (System.Runtime.InteropServices.COMException e) {
                    status = false;
                }
            } else {
                status = false;
            }

            return status;
        }

        public bool disableUser(string name) {

            bool status = false;
            string distinguishedName = getDistinguishedName(name);

            if (distinguishedName != "not Found") {

                try {
                    DirectoryEntry dirEntery = new DirectoryEntry($"LDAP://{distinguishedName}");
                    DirectorySearcher dirSearcher = new DirectorySearcher(dirEntery);

                    foreach (DirectoryEntry child in dirEntery.Children) {

                        if ((string)child.Properties["SamAccountName"].Value == name) {
                            int val = (int)child.Properties["userAccountControl"].Value;
                            child.Properties["userAccountControl"].Value = val | 0x2;
                            child.CommitChanges();
                            status = true;
                        }
                    }
                    dirSearcher.Dispose();
                } catch (System.Runtime.InteropServices.COMException e) {
                    status = false;
                }
            } else {
                status = false;
            }

            return status;
        }

        public bool isUserValid(string user) {
            var entry = new DirectoryEntry("LDAP://SDI.lab/DC=SDI,DC=lab");
            var searcher = new DirectorySearcher(entry) { Filter = $"SamAccountName={user}" };

            try {
                var searchResult = searcher.FindOne();

                if (searchResult != null) {

                    return true;
                } else {
                    return false;
                }

            } catch (System.Runtime.InteropServices.COMException exception) {
                return false;
            }

            entry.Close();
            searcher.Dispose();
        }


        private string getDistinguishedName(string userName) {

            var entry = new DirectoryEntry("LDAP://SDI.lab/DC=SDI,DC=lab");
            var searcher = new DirectorySearcher(entry) { Filter = $"SamAccountName={userName}" };

            try {
                var searchResult = searcher.FindOne();

                if (searchResult != null) {

                    string distinguishedName = searchResult.Properties["distinguishedName"][0].ToString();

                    if (distinguishedName.Contains("CN")) {
                        var distinguishedNameWithCn = distinguishedName.Split(new[] { ',' }, 2);
                        distinguishedName = distinguishedNameWithCn[1];
                    }
                    return distinguishedName;
                } else {
                    return "not Found";
                }

            } catch (System.Runtime.InteropServices.COMException exception) {
                return "not Found";
            }

        }
    }
}
