﻿using ADServices.Data.Repositories;
using System.DirectoryServices;

namespace ADServices.Data.Impl {
    class GroupsUsersRepository : IGroupsUsersRepository {

        public bool IsGroupValid(string groupName) {
            var entry = new DirectoryEntry("LDAP://SDI.lab/DC=SDI,DC=lab");
            var searcher = new DirectorySearcher(entry) { Filter = $"SamAccountName={groupName}" };

            try {
                var searchResult = searcher.FindOne();

                if (searchResult != null) {

                    return true;
                } else {
                    return false;
                }

            } catch (System.Runtime.InteropServices.COMException exception) {
                return false;
            }

            entry.Close();
            searcher.Dispose();
        }

        public bool addToGroup(string name, string groupName) {
            bool status;
            string groupDn = getDistinguishedNameWithCN(groupName);
            string userDn = getDistinguishedNameWithCN(name);

            if (groupDn != "not Found" && userDn != "not Found") {
                try {
                    DirectoryEntry dirEntry = new DirectoryEntry("LDAP://" + groupDn);
                    DirectorySearcher searcher = new DirectorySearcher(dirEntry);
                    dirEntry.Properties["member"].Add(userDn);
                    dirEntry.CommitChanges();
                    dirEntry.Close();
                    searcher.Dispose();
                    status = true;
                } catch (System.Runtime.InteropServices.COMException e) {
                    status = false;
                }
            } else {
                status = false;
            }
            return status;
        }

        public bool removeFromGroup(string name, string groupName) {
            bool status;
            string groupDn = getDistinguishedNameWithCN(groupName);
            string userDn = getDistinguishedNameWithCN(name);

            if (groupDn != "not Found" && userDn != "not Found") {
                try {
                    DirectoryEntry dirEntry = new DirectoryEntry("LDAP://" + groupDn);
                    DirectorySearcher searcher = new DirectorySearcher(dirEntry);
                    dirEntry.Properties["member"].Remove(userDn);
                    dirEntry.CommitChanges();
                    dirEntry.Close();
                    searcher.Dispose();
                    status = true;
                } catch (System.Runtime.InteropServices.COMException e) {
                    status = false;
                }
            } else {
                status = false;
            }
            return status;
        }

        public string getDistinguishedNameWithCN(string userName) {

            var entry = new DirectoryEntry("LDAP://SDI.lab/DC=SDI,DC=lab");
            var searcher = new DirectorySearcher(entry) { Filter = $"SamAccountName={userName}" };

            try {

                var searchResult = searcher.FindOne();
                if (searchResult != null) {
                    string distinguishedName = searchResult.Properties["distinguishedName"][0].ToString();
                    return distinguishedName;
                } else {
                    return "not Found";
                }
            } catch (System.Runtime.InteropServices.COMException exception) {
                return "not Found";
            }
            entry.Close();
            searcher.Dispose();
        }

        private bool groupMembers(string groupName, string userDn) {
            string allMembers = "";
            var entry = new DirectoryEntry("LDAP://SDI.lab/DC=SDI,DC=lab");
            var searcher = new DirectorySearcher(entry) { Filter = $"SamAccountName={groupName}" };
            try {
                var searchResult = searcher.FindOne();

                if (searchResult != null) {
                    foreach (string member in searchResult.Properties["member"]) {
                        allMembers += " " + member;
                    }
                    if (allMembers.Contains(userDn)) {
                        return true;
                    }
                }

            } catch (System.Runtime.InteropServices.COMException exception) {
                return false;
            }
            return false;

            entry.Close();
            searcher.Dispose();
        }

    }
}
