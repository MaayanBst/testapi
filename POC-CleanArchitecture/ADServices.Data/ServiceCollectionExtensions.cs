﻿using ADServices.Data.Repositories;
using ADServices.Data.Impl;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddSingleton<IUsersRepository, UsersRepository>();
            services.AddSingleton<IGroupsUsersRepository, GroupsUsersRepository>();
            services.AddSingleton<IOUUsersReposiroty, OUUsersReposiroty>();

            return services;
        }
    }
}
