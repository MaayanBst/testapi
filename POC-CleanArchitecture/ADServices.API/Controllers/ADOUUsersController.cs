﻿using ADServices.API.DTO;
using ADServices.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ADServices.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ADOUUsersController : ControllerBase
    {
        private readonly ILogger<ADOUUsersController> _logger;
        private readonly IADUsersService _adUsersService;
        private readonly IADOUUsersService _adOUUsersService;

        public ADOUUsersController(ILogger<ADOUUsersController> logger,
                                   IADOUUsersService adOuUsersService,
                                   IADUsersService adUsersService) {
            _logger = logger;
            _adUsersService = adUsersService;
            _adOUUsersService = adOuUsersService;
        }

        [HttpPut("Hierarchy")]
        public IActionResult Put([FromBody] userToOuDto userToOuDto)
        {
            if (!_adUsersService.IsValidUser(userToOuDto.name) || !_adOUUsersService.IsValidOu(userToOuDto.ouName)) {
                return BadRequest($"user:{userToOuDto.name} is not a valid user for the given operation");
            }

            if (!_adOUUsersService.hierarchy(userToOuDto.name, userToOuDto.ouName)) {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }

            return Ok();
        }

    }
}
