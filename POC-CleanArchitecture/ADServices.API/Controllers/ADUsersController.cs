﻿using ADServices.API.DTO;
using ADServices.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ADServices.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ADUsersController : ControllerBase
    {
        private readonly ILogger<ADUsersController> _logger;
        private readonly IADUsersService _adUsersService;

        public ADUsersController(ILogger<ADUsersController> logger, IADUsersService adUsersService)
        {
            _logger = logger;
            _adUsersService = adUsersService;
        }

        [HttpPut("displayName")]
        public IActionResult displayName([FromBody] userAttoributeUpdaetDto updateUserDto) {

            if (!_adUsersService.IsValidUser(updateUserDto.name)) {
                return BadRequest($"user:{updateUserDto.name} is not a valid user for the given operation");
            }

            if (!_adUsersService.updateDisplayName(updateUserDto.name, updateUserDto.attributeVal, updateUserDto.attribute)) {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }

            return Ok();
        }

        [HttpPut("status")]
        public IActionResult disable([FromBody] userStatusDto userStatusDto) {

            if (!_adUsersService.IsValidUser(userStatusDto.name)) {
                return BadRequest($"user:{userStatusDto.name} is not a valid user for the given operation");
            }

            if (!_adUsersService.updateStatus(userStatusDto.name, userStatusDto.status)) {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }

            return Ok();
        }
    }
}
