﻿using Microsoft.AspNetCore.Mvc;
using ADServices.API.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using ADServices.Services;

namespace ADServices.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ADGroupUsersController : ControllerBase
    {
        private readonly ILogger<ADGroupUsersController> _logger;
        private readonly IADGroupsUsersService _adGroupUsers;
        private readonly IADUsersService _adUsersService;

        public ADGroupUsersController(ILogger<ADGroupUsersController> logger,
                                      IADGroupsUsersService adGroupUsersService,
                                      IADUsersService adUsersService) {
            _logger = logger;
            _adGroupUsers = adGroupUsersService;
            _adUsersService = adUsersService;
        }

        [HttpPut("add")]
        public IActionResult add([FromBody] userToGroupDto userToGroupDto)
        {
            if (!_adUsersService.IsValidUser(userToGroupDto.name) || !_adGroupUsers.IsValidGroup(userToGroupDto.groupName)) {
                return BadRequest($"user:{userToGroupDto.name} is not a valid user for the given operation");
            }

            if (!_adGroupUsers.addToGroup(userToGroupDto.name, userToGroupDto.groupName)) {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }

            return Ok();
        }

        // DELETE api/<ADGroupUsers>/5
        [HttpDelete("remove")]
        public IActionResult remove([FromBody] userToGroupDto userToGroupDto)
        {
            if (!_adUsersService.IsValidUser(userToGroupDto.name) || !_adGroupUsers.IsValidGroup(userToGroupDto.groupName)) {
                return BadRequest($"user:{userToGroupDto.name} is not a valid user for the given operation");
            }

            if (!_adGroupUsers.removeFromGroup(userToGroupDto.name, userToGroupDto.groupName)) {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }

            return Ok();
        }
    }
}
