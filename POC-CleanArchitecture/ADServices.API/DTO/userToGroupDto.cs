﻿namespace ADServices.API.DTO {
    public class userToGroupDto {
        public string name { get; set; }
        public string groupName { get; set; }
    }
}
