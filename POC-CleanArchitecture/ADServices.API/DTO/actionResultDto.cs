﻿namespace ADServices.API.DTO
{
    public class actionResultDto
    {
        public string actionName { get; set; }

        public bool status { get; set; }

        public string log { get; set; }
    }
}
