﻿using ADServices.Services;
using ADServices.Services.Imp;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddSingleton<IADUsersService, ADUsersService>();
            services.AddSingleton<IADGroupsUsersService, ADGroupsUsersService>();
            services.AddSingleton<IADOUUsersService, ADOUUsersService>();

            return services;
        }
    }
}
