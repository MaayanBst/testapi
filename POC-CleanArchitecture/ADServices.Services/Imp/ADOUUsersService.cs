﻿using ADServices.Data.Repositories;
using Microsoft.Extensions.Logging;

namespace ADServices.Services.Imp {
    class ADOUUsersService : IADOUUsersService {

        private readonly ILogger<ADOUUsersService> _logger;
        private readonly IOUUsersReposiroty _ouUsersRepository;

        public ADOUUsersService(ILogger<ADOUUsersService> logger, IOUUsersReposiroty ouUsersRepository) {
            _logger = logger;
            _ouUsersRepository = ouUsersRepository;
        }

        public bool hierarchy(string name, string ouName) {
            return _ouUsersRepository.hierarchy(name, ouName);
        }

        public bool IsValidOu(string ouName) {
            return _ouUsersRepository.isValidOu(ouName);
        }
    }
}
