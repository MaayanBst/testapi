﻿using ADServices.Data.Repositories;
using Microsoft.Extensions.Logging;

namespace ADServices.Services.Imp
{
    class ADUsersService : IADUsersService
    {
        private readonly ILogger<ADUsersService> _logger;
        private readonly IUsersRepository _usersRepository;

        public ADUsersService(ILogger<ADUsersService> logger, IUsersRepository usersRepository)
        {
            _logger = logger;
            _usersRepository = usersRepository;
        }
        
        
        public bool IsValidUser(string user)
        {
            return _usersRepository.isUserValid(user);
        }

        public bool updateDisplayName(string name, string attributeVal, string attribute) {

            return _usersRepository.updateAttribute(name, attributeVal, attribute);
        }

        public bool updateStatus(string name, bool status) {
            
            if (status) {
                return _usersRepository.enableUser(name);
            } else {
                return _usersRepository.disableUser(name);
            }
        }
    }
}
