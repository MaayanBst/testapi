﻿using Microsoft.Extensions.Logging;
using ADServices.Data.Repositories;

namespace ADServices.Services.Imp {
    class ADGroupsUsersService : IADGroupsUsersService {

        private readonly ILogger<ADGroupsUsersService> _logger;
        private readonly IGroupsUsersRepository _groupsUsersRepository;

        public ADGroupsUsersService(ILogger<ADGroupsUsersService> logger, IGroupsUsersRepository groupsUsersRepository) {
            _logger = logger;
            _groupsUsersRepository = groupsUsersRepository;
        }

        public bool addToGroup(string name, string groupName) {
            return _groupsUsersRepository.addToGroup(name, groupName);
        }

        public bool IsValidGroup(string groupName) {

            return _groupsUsersRepository.IsGroupValid(groupName);
        }

        public bool removeFromGroup(string name, string groupName) {
            return _groupsUsersRepository.removeFromGroup(name, groupName);
        }
    }
}
