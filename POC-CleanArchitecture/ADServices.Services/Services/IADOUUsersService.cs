﻿namespace ADServices.Services {
    public interface IADOUUsersService {
        bool IsValidOu(string ouName);
        bool hierarchy(string name, string ouName);
    }
}
