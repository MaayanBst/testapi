﻿namespace ADServices.Services
{
    public interface IADUsersService
    {
        bool IsValidUser(string user);
        bool updateDisplayName(string name, string attributeVal, string attribute);
        bool updateStatus(string name, bool status);
    }
}
