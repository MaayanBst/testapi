﻿namespace ADServices.Services {
    public interface IADGroupsUsersService {
        bool IsValidGroup(string groupName);
        bool addToGroup(string name, string groupName);
        bool removeFromGroup(string name, string groupName);
    }
}
